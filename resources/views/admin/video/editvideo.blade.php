@extends('partials._design')

@section('content')

<div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                	Add Data Video dan Animasi
                	<a style="float: right;" class="btn btn-primary" href="">Back</a>
            	</div>

                <div class="card-body">
                    
                   <form class="form-horizontal" enctype="multipart/form-data" action="{{url('video/update')}}/{{$video->id}}" method="POST"  id="videoForm">
                        
                        <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
                            <label for="judul" class="col-md-4 control-label">Judul</label>

                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="judul" value="{{ @$video->judul }}" required>

                                @if ($errors->has('judul'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('judul') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('sumber') ? ' has-error' : '' }}">
                            <label for="sumber" class="col-md-4 control-label">Sumber</label>

                            <div class="col-md-6">
                                <input id="sumber" type="text" class="form-control" name="sumber" value="{{ @$video->sumber }}" required>

                                @if ($errors->has('sumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <label for="file" class="col-md-4 control-label">File Video</label>

                            <div class="col-md-6">
                                <input id="file" type="text" class="form-control" name="file" value="{{ @$video->file }}" required>

                                @if ($errors->has('file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
                            <label for="keterangan" class="col-md-4 control-label">Keterangan</label>

                            <div class="col-md-6">
                                <textarea id="keterangan" type="text" class="form-control" name="keterangan" required>{{ @$video->keterangan }}</textarea>

                                @if ($errors->has('keterangan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('keterangan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                
                                <button type="submit" class="btn btn-primary"> Submit
                                </button>
                                 {{ csrf_field() }}
                            </div>
                        </div>
                    </form>
                    <video width="320" height="240" controls>
                      <source src="{{ asset('storage/videos/'.$video->file) }}" type="video/mp4">
                      <source src="{{ asset('storage/videos/'.$video->file) }}" type="video/3gp">
                      Your browser does not support the video tag.
                    </video>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('partials._design')

@section('content')
<script type="text/javascript">
    var baseImgpath = "{{ asset('storage/images') }}"
</script>
<div class="container" id="modul-content">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                	Add Data Modul
                	<a style="float: right;" class="btn btn-primary" href="{{ route('modul.index') }}">Back</a>
            	</div>

                <div class="card-body">
                    
                   <!--  @if(@$modul->exists) <?php  $url //= route('modul.update', @$modul->id); ?>
                    @else <?php  $url //= route('modul.store'); ?>
                    @endif -->
                	<form class="form-horizontal" action="{{ route('modul.store') }}" method="POST" enctype="multipart/form-data" onsubmit="return getContent();" id="modulForm">
                        
                      <!--   @if (@$model->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif
 -->
                        {{ csrf_field() }}

                        <!-- <div class="form-group{{ $errors->has('nomor') ? ' has-error' : '' }}">
                            <label for="bab" class="col-md-4 control-label">Bab</label>

                            <div class="col-md-6">
                                <input id="bab" type="text" class="form-control" name="bab" value="{{ @$modul->bab }}" required>

                                @if ($errors->has('bab'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bab') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->

                        <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
                            <label for="judul" class="col-md-4 control-label">Judul</label>

                            <div class="col-md-6">
                                <input id="judul" type="text" class="form-control" name="judul" value="{{ @$modul->judul }}" required>

                                @if ($errors->has('judul'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('judul') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('sumber') ? ' has-error' : '' }}">
                            <label for="sumber" class="col-md-4 control-label">Sumber</label>

                            <div class="col-md-6">
                                <input id="sumber" type="text" class="form-control" name="sumber" value="{{ @$modul->sumber }}" required>

                                @if ($errors->has('sumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('konten') ? ' has-error' : '' }}">
                            <label for="konten" class="col-md-4 control-label">Konten</label>                   
                            <div class="col-lg-12">
                                <textarea class="form-control" rows="8" id="editor1" name="konten"></textarea>

                                @if ($errors->has('konten'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('konten') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" id="uploadgambar">
                                <div class="form-group">
                                    <label class="form-control-label" for="gambar">Tambah File Gambar</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="custom-file">
                                                <input type="file" name="file[]" class="custom-file-input change-1" id="customFileLang" onchange="chooseValue(1)" lang="en">
                                                <label class="custom-file-label label-change-1" for="customFileLang">Pilih Gambar</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group float-right">
                                    <button class="btn btn-icon btn-info" type="button" id="add_button">
                                        <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                                        <span class="btn-inner--text">Tambah Gambar</span>
                                    </button>
                                    <button type="submit" class="btn btn-primary">Submit
                                            <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                                        <!--  @if (@$modul->exists)
                                             Update 
                                         @else
                                             Add 
                                         @endif -->
                                     </button>
                                     {{csrf_field()}}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script type="application/javascript">
        $(document).ready(function () {
            var i=0
            $('#add_button').on('click',function () {
                i++
                $('#uploadgambar').append('<div class="form-group">\n' +
                    '    <div class="input-group">\n' +
                    '        <div class="input-group-prepend">\n' +
                    '            <div class="custom-file">\n' +
                    '                <input type="file" name="file[]" class="custom-file-input change-'+(i+1)+'" onchange="chooseValue('+(i+1)+')"  lang="en">\n' +
                    '                <label class="custom-file-label label-change-'+(i+1)+'" for="customFileLang">Tambah Gambar</label>' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '    </div>\n' +
                    '</div>');
            })
        })
    </script>
    <script>
        function chooseValue(i){
            var fileName=$('.change-'+i).val()
            fileName=fileName.substring(fileName.lastIndexOf("\\")+1,fileName.length)
            $(".label-change-"+i).html(fileName)

        }
    </script>
        
@endsection

@endsection

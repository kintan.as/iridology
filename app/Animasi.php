<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animasi extends Model
{
    protected $fillable = [
		'nomor',
		'judul',
		'sumber',
		'file',
	];
}

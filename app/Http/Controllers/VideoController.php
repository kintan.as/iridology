<?php

namespace App\Http\Controllers;

use App\Video;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;

class VideoController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $video = Video::all();
        return view('admin.video.listvideo', ['video' => $video]);

    }

    // *
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
     
    public function create()
    {
        $video= Video::all();
        return view('admin.video.formvideo');

    }


    public function store(Request $request)
    {
        $video = new Video();
    	$video->judul = $request->judul;
    	$video->sumber = $request->sumber;
        $video->keterangan = $request->keterangan;
        // $video->file = null;

        // dd($video);

    	$allowedfileExtension=['3gp','mp4','webm'];
        $file = $request->file;
    	// $path_parts = pathinfo($video->file);
        $extension = strtolower($file->getClientOriginalExtension());

    	if (in_array($extension, $allowedfileExtension)) {
            $date = date_create();
            $filename = date_timestamp_get($date) .'.'. $extension;
            $video->file = $file->storeAs('public/videos', $filename);
    		$video->save();
            return redirect('video/');
        } else {
            return redirect('/video')->with('failed', 'Video has not been added');
        }
        // if(!is_null($video->file)){
        //     if(is_null($request->file('file')) && $request->file('file')==''){
        //         $fileImageFile = $video->file;
        //     }else{
        //         $rules = [
        //             'file' => 'mimes:video/mp4, video/3gpp',
        //         ];
        //         $validator = Validator::make($request->all(), $rules);
        //         if($validator->fails())
        //         {
        //             return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
        //         } else
        //         {
        //             $fotoImageFile = $request->file('file');
        //             $destinationPath = 'public/videos/'.$request->image_file_id;
        //             if (!file_exists($destinationPath)) {
        //                 File::makeDirectory($destinationPath, $mode = 0777, true, true);
        //             }
        //             $fileImageFile = date('YmdHis') . '_' . $fotoImageFile->getClientOriginalName();
        //             $fotoImageFile->move($destinationPath, $fileImageFile);
        //         }
        //     }
        // }

        // if(is_null($video->file)){
        //     if(is_null($request->file('file')) && $request->file('file')==''){
        //         $fileImageFile = $video->file;
        //     }else{
        //         $rules = [
        //             'file' => 'mimes:video/mp4, video/3gpp',
        //         ];
        //         $validator=Validator::make($request->all(), $rules);
        //         if($validator->fails())
        //         {
        //             return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
        //         } else
        //         {
        //             $fotoImageFile = $request->file('file');
        //             $destinationPath = 'public/videos/'.$request->image_file_id;
        //             if (!file_exists($destinationPath)) {
        //                 File::makeDirectory($destinationPath, $mode = 0777, true, true);
        //             }
        //             $fileImageFile = date('YmdHis') . '_' . $fotoImageFile->getClientOriginalName();
        //             $fotoImageFile->move($destinationPath, $fileImageFile);
        //         }
        //     }
        // }
        // $video->file = $file->storeAs('public/videos', $fileImageFile);

    	// try {
        //     $video->save();
        //     return redirect('/video')->with('success', 'Video has been added');
        // } catch (\Exception $ex){
        //     return redirect('/video')->with('failed', 'Video has not been added');
        // }
    }

    public function edit($id)
    {
        $video = Video::find($id);
        $video->file = explode("/", $video->file)[2];
        return view('admin.video.editvideo', ['video' => $video]);

    }

    public function update(Request $request, $id)
    {
       // $request->konten = trim($request->konten, "");
        $request->validate([
            'judul' => 'required',
            'sumber' => 'required',
            'file' => 'required',
            'keterangan' => 'required',
        ]);
        // var_dump(trim($request->konten));
        // die();
        $video = video::find($id);
        $video->judul = $request->judul;
        $video->sumber = $request->sumber;
        $video->keterangan = $request->keterangan;
        $allowedfileExtension=['3gp','mp4'];
    	$path_parts = pathinfo($video->file);
    	if (in_array($path_parts['extension'], $allowedfileExtension)) {
    		$video->save();
            return redirect('video');
    	}
    	else{
    		return redirect()->back()->withErrors('Format video tidak didukung');
    	}
    }

    public function destroy($id)
    {
         $video = video::find($id);
         Storage::delete($video->file);
         $video -> delete();
        return redirect('video')->with('alert-success','Berhasil Menghapus Data!'); 

    }

    public function get()
    {
        $video = new video;
        $video->results= video::all();
        return response()
            ->json($video, 200);
    }

     public function get_id($id){
        $video = new video;
        $video->result = video::find($id);
        return response()->json($video, 200);
    }
}
